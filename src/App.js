import { useState } from 'react';
import CountDown from './components/CountDown';

import './App.css';

function App() {
  const [initialCount, setInitialCount] = useState(0);

  return (
    <div className="container">
      <div className="controllers">
        <button
          className="button"
          onClick={() => {
            if (initialCount < 1) return;
            setInitialCount(initialCount - 1);
          }}
        >
          -
        </button>
        <h1>{`${initialCount} Minute`}</h1>
        <button
          className="button"
          onClick={() => {
            if (initialCount > 4) return;
            setInitialCount(initialCount + 1);
          }}
        >
          +
        </button>
      </div>
      {initialCount ? (
        <CountDown
          initialCount={initialCount}
          setInitialCount={setInitialCount}
        />
      ) : null}
    </div>
  );
}

export default App;
