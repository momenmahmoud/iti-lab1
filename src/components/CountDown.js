import { useEffect, useState } from 'react';

const CountDown = ({ initialCount, setInitialCount }) => {
  const [counter, setCounter] = useState();

  useEffect(() => {
    setCounter(initialCount * 60);
  }, [initialCount]);

  useEffect(() => {
    if (counter === 0) setInitialCount(0);
  }, [counter, setInitialCount]);

  useEffect(() => {
    const calculateChange = (prevCount) => {
      console.log(prevCount);
      return prevCount - 1;
    };
    const countDown = setInterval(() => {
      setCounter(calculateChange);
    }, 1000);

    return () => {
      clearInterval(countDown);
    };
  }, []);

  return <h1>{`${counter} Second`}</h1>;
};

export default CountDown;
